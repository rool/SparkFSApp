#
# Copyright (c) 2023, RISC OS Open Ltd
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of RISC OS Open Ltd nor the names of its contributors
#       may be used to endorse or promote products derived from this software
#       without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
#
# Makefile for SparkFS (filer front end)
#

COMPONENT  = SparkFSApp
INSTAPP    = ${INSTDIR}${SEP}!SparkFS
TARGET     = !RunImage
INSTTYPE   = app
CFLAGS     = -c90
CINCLUDES  = ${RINC}
OBJS       = code codea2b codeuu codehqx compress deboo demime file \
             fileract flex fs fsx info main md5 memory mimecode mkboo \
             mkmime mym ram sparkfs task trans wos wz swi poll
INSTAPP_FILES = !Boot !Run !RunImage \
                Sprites Sprites22 \
                Sprites:Resources Sprites22:Resources ResFind:Resources UK.Menu:Resources \
                Themes.!Sprites:Themes Themes.!Sprites22:Themes [Themes.!Sprites11:Themes] \
                Themes.Ursula.!Sprites:Themes.Ursula Themes.Ursula.!Sprites22:Themes.Ursula \
                Themes.Morris4.!Sprites:Themes.Morris4 Themes.Morris4.!Sprites22:Themes.Morris4 \
                AutoRun.Zip:AutoRun \
                Config.AtExit:Config Config.Choices:Config Config.Extensions:Config Config.NoCo:Config
INSTAPP_DEPENDS = France Germany Netherland UK

include CApp

# SparkFS is unusual in that it carries 4 sets of translations
France Germany Netherland UK:
	${MKDIR} ${INSTAPP}${SEP}Resources${SEP}$@
	${CP} Resources${SEP}$@ ${INSTAPP}${SEP}Resources${SEP}$@ ${CPFLAGS}
	${WIPE} ${INSTAPP}${SEP}Resources${SEP}$@${SEP}Messages ${WFLAGS}
	${INSERTVERSION} Resources${SEP}$@${SEP}Messages > ${INSTAPP}${SEP}Resources${SEP}$@${SEP}Messages

# Dynamic dependencies:
